# debuginfo doesn't work yet
%global debug_package %{nil}

Epoch: 2
Name: netavark
Version: 1.1.0
Release: 6%{?dist}
License: ASL 2.0 and BSD and MIT
ExclusiveArch: %{rust_arches}
Summary: OCI network stack
URL: https://github.com/containers/%{name}
Source0: %{url}/archive/v%{version}/%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-v%{version}-vendor.tar.gz
Source2: netavark.1
BuildRequires: cargo
Recommends: aardvark-dns >= 1.0.3
Provides: container-network-stack = 2
BuildRequires: make
BuildRequires: rust-srpm-macros
BuildRequires: git-core

%description
%{summary}

Netavark is a rust based network stack for containers. It is being
designed to work with Podman but is also applicable for other OCI
container management applications.

Netavark is a tool for configuring networking for Linux containers.
Its features include:
* Configuration of container networks via JSON configuration file
* Creation and management of required network interfaces,
    including MACVLAN networks
* All required firewall configuration to perform NAT and port
    forwarding as required for containers
* Support for iptables and firewalld at present, with support
    for nftables planned in a future release
* Support for rootless containers
* Support for IPv4 and IPv6
* Support for container DNS resolution via aardvark-dns.

%prep
%autosetup -Sgit
tar fx %{SOURCE1}
mkdir -p .cargo

cat >.cargo/config << EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

%build
%{__make} build

cd docs
cp %{SOURCE2} .

%install
%{__make} DESTDIR=%{buildroot} PREFIX=%{_prefix} install

%files
%license LICENSE
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/%{name}
%{_mandir}/man1/%{name}.1*

%changelog
* Tue Aug 09 2022 Jindrich Novy <jnovy@redhat.com> - 2:1.1.0-6
- bump Epoch to preserve upgrade path
- Related: #2061390

* Tue Aug 09 2022 Jindrich Novy <jnovy@redhat.com> - 1.1.0-5
- remove dependency on md2man
- Related: #2061390

* Tue Aug 09 2022 Jindrich Novy <jnovy@redhat.com> - 1.1.0-4
- fix arches
- Related: #2061390

* Tue Aug 09 2022 Jindrich Novy <jnovy@redhat.com> - 1.1.0-3
- add gating.yaml
- Related: #2061390

* Thu Aug 04 2022 Jindrich Novy <jnovy@redhat.com> - 1.1.0-2
- require /usr/bin/go-md2man directly

* Wed Aug 03 2022 Jindrich Novy <jnovy@redhat.com> - 1.1.0-1
- initial import
- Related: #2061390
